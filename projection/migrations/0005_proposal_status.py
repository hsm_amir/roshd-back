# Generated by Django 3.2.2 on 2021-06-05 17:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projection', '0004_auto_20210528_1221'),
    ]

    operations = [
        migrations.AddField(
            model_name='proposal',
            name='status',
            field=models.PositiveSmallIntegerField(choices=[(1, 'registrated'), (2, 'agreed'), (3, 'against'), (4, 'present'), (5, 'accepted'), (6, 'rejected'), (7, 'wait_for_sign'), (8, 'signed')], default=1, help_text='status of proposal'),
        ),
    ]
