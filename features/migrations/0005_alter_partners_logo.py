# Generated by Django 3.2.2 on 2021-05-17 12:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('features', '0004_alter_slider_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partners',
            name='logo',
            field=models.ImageField(blank=True, null=True, upload_to='partners'),
        ),
    ]
